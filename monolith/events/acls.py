from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}

def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    us_ISO = "ISO 3166-2:US"
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{us_ISO}&limit={1}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response_geo = requests.get(geocode_url)
    # Parse the JSON response
    content_geo = json.loads(response_geo.content)
    # Get the latitude and longitude from the response
    lat = content_geo["lat"]
    lon = content_geo["lon"]

    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response_weather = requests.get(weather_url)
    # Parse the JSON response
    content_weather = json.loads(response_weather)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    weather_data = {
        "temp": content_weather["main"]["temp"],
        "description": content_weather["weather"]["description"],
    }
    # Return the dictionary
    try:
        return weather_data
    except:
        return {
            "temp": None,
            "description": None,
        }
